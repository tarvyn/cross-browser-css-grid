import { Component } from '@angular/core';
import DATA from './MOCK_DATA.json';

interface Contact {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  gender: string;
  ip_address: string;
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'css-grid';
  data: Array<Contact> = DATA;

  getMsStyles(columnIndex: number, rowIndex: number): {[style: string]: string} {
    return {
      '-ms-grid-column': columnIndex.toString(),
      '-ms-grid-row': rowIndex.toString(),
    };
  }
}
